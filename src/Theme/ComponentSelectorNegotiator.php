<?php

namespace Drupal\component_selector\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Defines a theme negotiator that deals with the active theme on the component
 * selector page.
 */
class ComponentSelectorNegotiator implements ThemeNegotiatorInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ComponentSelectorNegotiator.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The request stack used to retrieve the current request.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {    
    return $route_match->getRouteName() == 'component_selector.component_selector';
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {    
    // Returns the default theme.
    $config = $this->configFactory->get('system.theme');
    return $config->get('default');
  }

}
