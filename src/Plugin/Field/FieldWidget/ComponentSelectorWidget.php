<?php

namespace Drupal\component_selector\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenOffCanvasDialogCommand;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;

/**
 * Plugin implementation of the 'entity_reference_revisions paragraphs' widget.
 *
 * @FieldWidget(
 *   id = "component_selector",
 *   label = @Translation("Component Selector IEF"),
 *   description = @Translation("A component selector widget."),
 *   field_types = {
 *     "entity_reference_revisions",
 *     "entity_reference"
 *   },
 *   multiple_values = true
 * )
 */
class ComponentSelectorWidget extends InlineEntityFormComplex {
 
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);  

    // Only show the component selector options when no form is visible.
    $open_form = $form_state->get(['inline_entity_form', $this->getIefId(), 'form']);
    if (empty($open_form)) {
      $this->buildComponentSelectorForm($element);
    }

    return $element;
  }

  /**
   * Builds the element form widget for the component selector.
   */
  private function buildComponentSelectorForm(array &$element) {    
    $entity_type = $this->getFieldSetting('target_type');
    $bundles = $this->getTargetBundles();

    $iframe_link = Url::fromRoute(
      'component_selector.component_selector',
      ['entity_type' => $entity_type],
      [
        'query' => ['bundle' => $bundles],
        'absolute' => TRUE,
      ]
    )->toString();

    $element['actions']['styleguide'] = [
      '#type' => 'submit',
      '#name' => 'ief-' . $this->getIefId() . '-styleguide',
      '#value' => $this->t('Add Component'),     
      '#ajax' => [
        'callback' => [static::class, 'addComponentCallback'],
      ],
      '#iframe_link' => $iframe_link,
    ];

    $element['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $element['#attached']['library'][] = 'core/drupal.dialog.off_canvas';
    $element['#attached']['library'][] = 'component_selector/component_selector.widget';

    // Adding classes and hiding the IEF buttons.
    $element['actions']['bundle']['#attributes']['class'][] = $this->getComponentSelectorCssClass('bundle', $element['#ief_id']);
    $element['actions']['bundle']['#attributes']['class'][] = 'visually-hidden';

    $element['actions']['ief_add']['#attributes']['class'][] = $this->getComponentSelectorCssClass('add', $element['#ief_id']);
    $element['actions']['ief_add']['#attributes']['class'][] = 'visually-hidden';
  }

  /**
   * Ajax callback for the "Add Component" button.
   *
   * This returns the desired page in an iframe withing the off_canvas dialog.
   */
  public function addComponentCallback(array $form, FormStateInterface $form_state) {    
    $element = inline_entity_form_get_element($form, $form_state);

    $iframe_link = $element['actions']['styleguide']['#iframe_link'];

    $bundle_css_class = '.' . static::getComponentSelectorCssClass('bundle', $element['#ief_id']);
    $add_css_class = '.' . static::getComponentSelectorCssClass('add', $element['#ief_id']);

    // @todo convert this to an actual theme hook.
    $markup = [    
      '#type' => 'inline_template',
      '#template' => '<iframe class="component-selector" src="{{ url }}" data-cs-bundle="{{ cs_bundle }}" data-cs-add="{{ cs_add }}"></iframe>',
      '#context' => [
        'url' => $iframe_link,
        'cs_bundle' => $bundle_css_class,
        'cs_add' => $add_css_class,
      ],
    ];

    $response = new AjaxResponse();    
    $response->addCommand(new OpenOffCanvasDialogCommand('Component Preview',  $markup, ['width' => 700]));
    
    return $response;
  }
 
  /**
   * Helper method to generate CSS classes to use for the component selector.
   */
  private static function getComponentSelectorCssClass($field, $ief_id) {    
    return sprintf(
      'cs-%s-%s',
      $field,
      $ief_id
    );
  }  
}
