<?php

namespace Drupal\component_selector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\paragraphs\Entity\Paragraph;


/**
 * Returns responses for ComponentSelector routes.
 */
class ComponentSelectorController extends ControllerBase {
  
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new ComponentSelectorController.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack) {
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;    
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function build($entity_type) {
    $validEntityTypes = array_keys($this->entityTypeManager->getDefinitions());
    
    if (!in_array($entity_type, $validEntityTypes)) {
      // @todo Think of better ways to handle this.
      throw new \Exception('Invalid entity type: ' . $entity_type);
    }
    
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);

    // @todo Thoughts on how to render the above:
    // 1. Check the DB to see if there is one to load. If there is, load it and use it.
    // 2. If there isn't one, try to use the default field settings.
    // 3. If there are not default field settings, use the field labels. In the case of images / media, maybe some placeholder image?
    // 4. Show whatever we can and put up a warning message that it may not be the best representation, but will get better as the library is expanded?
    // 5. Perhaps add an image that can be rendered instead of live content?

    // Grabs any requested bundles from the DB.
    // Will grab the most recent componenent for each bundle (max id).
    $bundles = [];
    if ($this->requestStack->getCurrentRequest()->query->has('bundle')) {
      // @todo Will likely need to contend with 'bundle' vs 'type' if we are 
      // working with nodes.
      $bundles = $this->requestStack->getCurrentRequest()->query->get('bundle');
    }
    
    // Gets the highest id (most recent) of any existing paragraphs of the specified types.
    // @todo DI
    $query = \Drupal::entityQueryAggregate($entity_type);    
    $query->groupBy('type')->aggregate('id', 'MAX');
    
    if (!empty($bundles)) {
      $query->condition('type', $bundles, 'IN');
    }
    
    $result = $query->execute();
           
    foreach ($result as $type) {
      $component_ids[] = $type['id_max'];
    } 

    // @todo DI
    $components = \Drupal::entityManager()->getStorage($entity_type)->loadMultiple($component_ids);

    // dpm($components);

    foreach ($components as $component) {
      $entity = $view_builder->view($component);
      $label = $component->type->entity->label();

      $render_entities[] = [
        '#theme' => 'component_selector_preview',
        '#entity' => $entity,
        '#label' => $label,
        '#cs_bundle' => $component->getType(),
      ];
    } 

    return [
      '#theme' => 'component_selector',
      '#entities' => $render_entities,
      '#attached' => [
        'library' => [
          'component_selector/component_selector.page',
        ],
      ],
    ];
  }

}
