(function ($, Drupal) {

'use strict';

  Drupal.behaviors.component_selector_widget = {
    attach: function (context, settings) {
      
      if (!context.classList) {
        return;
      }
      
      if (!context.classList.contains('component-selector')) {
        return;
      }

      // TODO refactor this to use promises.
      var $iframe = $(context);
      $iframe.on('load', function() {
        console.log('iframe loaded');

        var $select_buttons = $('.cs-select-button', $iframe.contents());
        $select_buttons.on('click', function () {
          // Updates the selected bundle type.
          var selected_bundle = $(this).data('cs-bundle');
          var bundle_field = $($iframe.data('cs-bundle'));
          $(bundle_field).val(selected_bundle);

          // Submits the IEF form to add the component.
          // Note: The mousedown event is used because this is an ajac form and
          // the click event is prevented by core.
          var add = $($iframe.data('cs-add'));
          add.trigger('mousedown');

          // Closing the off_canvas dialog.
          $('.ui-dialog-off-canvas button.ui-dialog-titlebar-close').click();
        });
      });
    }
  };

})(jQuery, Drupal);